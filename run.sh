#!/usr/bin/bash

source setup.sh
cfgs=(channel_brem_63 inclusive_e+_e-)

for cfg in ${cfgs[@]}; do
    python validation.py -c share/${cfg}.yaml $@
done
