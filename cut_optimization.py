import ROOT
from ROOT import TMVA, TFile, TTree, TCut

# Initialize TMVA
TMVA.Tools.Instance()
TMVA.PyMethodBase.PyInitialize()

# Create output file
outputFile = TFile.Open("TMVA.root", "RECREATE")

# Create a factory object
factory = TMVA.Factory("TMVAClassification", outputFile,
                       "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification")

# Load data
dataLoader = TMVA.DataLoader("dataset")

# Create a ROOT TTree and fill it with some data (for demonstration purposes)
signalFile = TFile.Open("/lustre/collider/hepmc/DarkSHINE_Production/Baseline_1.6/e+_E4_W_noECALBField/signal_brem/dp_ana/merge_63MeV.root")
backgroundFile = TFile.Open("/lustre/collider/hepmc/DarkSHINE_Production/Baseline_1.6/e+_E4_W_noECALBField/inclusive/dp_ana/merge_0.root")

signalTree = signalFile.Get("dp")
backgroundTree = backgroundFile.Get("dp")

# Define the input variables that will be used for training
dataLoader.AddVariable("ECAL_E_total[0]", "F")
dataLoader.AddVariable("HCAL_E_Max_Cell[0]", "F")

# Add signal and background trees to dataloader
dataLoader.AddSignalTree(signalTree, 1.0)
dataLoader.AddBackgroundTree(backgroundTree, 1.0)

# Apply a cut on the training sample
mycut = TCut("")

# Prepare the training and testing data
dataLoader.PrepareTrainingAndTestTree(mycut, "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:NormMode=NumEvents:!V")

# Book the Rectangular Cut method
factory.BookMethod(dataLoader, TMVA.Types.kCuts, "RectangularCuts",
                   "!H:!V:FitMethod=GA:EffSel:VarProp=FSmart")

# Train, test, and evaluate the classifiers
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

# Save the output
outputFile.Close()

# Launch the GUI for the root file
ROOT.TMVA.TMVAGui("TMVA.root")
