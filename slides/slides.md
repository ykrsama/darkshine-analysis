---
theme: ./
layout: cover
class: text-left
transition: slide-left
mdc: true
backgroud: '/ATLAS/ATLAS-Logo.png'
authors:  # First author should be the presenter
  - Xuliang Zhu: ["TDLI"]
  - Tong Sun: ["TDLI"]
  - Yulei Zhang: ["INPAC"] 

meeting: "Dark SHINE Simulation and Analysis Meeting"
preTitle: "Positron Beam A' Update"
---

<br>

<img id="ATLAS" src="/DarkSHINE/DarkSHINE-Logo.png"> </img>

<style scoped>
#ATLAS {
  width: 160px;
  position: absolute;
  right: 2%;
  bottom: 2%;
  /* backgroond-color: #2B90B6;
  background-image: linear-gradient(45deg, #4EC5D4 15%, #146b8c 50%); */
}
</style>

---
layout: pageBar
hideInToc: true
---

# Outline

<br>

<div class="flex justify-center items-center" style="height: 50vh;">

### <Toc />

</div>

---
layout: pageBar
---

# Samples and Signal Region Definition

<br>

| Sample    | Process                                   | $m_{A'}$ (MeV)    | Event Number (per mass point) |
|-----------|-------------------------------------------|-------------------|-------------------------------|
| darkbrem  | ![img](/images/darkbrem.png){width=100px}, eBrem+conv+ | 1, 10, 50, 80, 90 | $5\times 10^{5}$              |
| t-channel | ![img](/images/tchan.png){width=100px}    | 1, 10, 50, 80, 90 | $5\times 10^{5}$              |
| s-channel | ![img](/images/schan.png){width=100px}    | 1, 10, 50, 80, 90 | $5\times 10^{5}$              |
| Inclusive | Background                                | -                 | $5\times 10^{5}$              |

<br>

| Signal Region | Tagging N<sub>Track</sub> | Recoil N<sub>Track</sub> | Missing P | ECAL E<sub>Total</sub> | HCAL E<sub>Max Cell</sub> |
|---------------|---------------------------|--------------------------|-----------|------------------------|---------------------------|
| 1             | == 1                      | == 1                     | > 4 GeV   | < 2.5 GeV              | < 5 MeV                |
| 2             | == 1                      | == 0                     | > 4 GeV   | > 0 GeV && < 6 GeV     | < 5 MeV                |
| 3             | == 1                      | == 0                     | > 4 GeV   |  == 0 GeV              | < 5 MeV                |


---
layout: pageBar
---

# Cutflow

m<sub>A'</sub> = 90 MeV, Signal Region 1, 2, 3


| Signal Efficiency | signal region 1 | signal region 2 | signal region 3 |
|-------------------|-----------------|-----------------|-----------------|
| darkbrem          | **56%**             | 21%             | 8.6%            |
| t-channel         | 1.4%            | **94%**             | 3.9%            |
| s-channel         | 0.49%           | 22%             | **76%**             |

<div grid="~ cols-3 gap-1">

<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel1_90MeV/plot/json/cutflow.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel2_90MeV/plot/json/cutflow.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel3_90MeV/plot/json/cutflow.json"></plotlygraph>
</Transform>
</div>


---
layout: pageBar
hideInToc: true
---

# Cutflow

m<sub>A'</sub> = 50 MeV, Signal Region 1, 2, 3

<div grid="~ cols-3 gap-1">

<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel1_50MeV/plot/json/cutflow.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel2_50MeV/plot/json/cutflow.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel3_50MeV/plot/json/cutflow.json"></plotlygraph>
</Transform>
</div>


---
layout: pageBar
hideInToc: true
---

# Cutflow

m<sub>A'</sub> = 1 MeV, Signal Region 1, 2, 3

<div grid="~ cols-3 gap-1">

<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel1_1MeV/plot/json/cutflow.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel2_1MeV/plot/json/cutflow.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel3_1MeV/plot/json/cutflow.json"></plotlygraph>
</Transform>
</div>

---
layout: pageBar
---

# Tracker N

m<sub>A'</sub> = 90, 50, 1 MeV

<div grid="~ cols-3 gap-1">

<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_90MeV/plot/json/RecTrk2_track_No__precut.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_50MeV/plot/json/RecTrk2_track_No__precut.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_1MeV/plot/json/RecTrk2_track_No__precut.json"></plotlygraph>
</Transform>
</div>

---
layout: pageBar
---

# Missing P vs ECAL E

m<sub>A'</sub> = 90, 50, 1 MeV

<div grid="~ cols-3 gap-1">

<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_90MeV/plot/json/ECAL_E_smear_vs_MissingP.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_50MeV/plot/json/ECAL_E_smear_vs_MissingP.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_1MeV/plot/json/ECAL_E_smear_vs_MissingP.json"></plotlygraph>
</Transform>
</div>

---
layout: pageBar
---

# HCAL E<sub>Max Cell</sub>

m<sub>A'</sub> = 90, 50, 1 MeV

<div grid="~ cols-3 gap-1">

<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel1_90MeV/plot/json/HCAL_E_Max_Cell_truth_cut4.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel1_50MeV/plot/json/HCAL_E_Max_Cell_truth_cut4.json"></plotlygraph>
</Transform>
<Transform :scale="0.45">
    <plotlygraph filePath="https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_channel1_1MeV/plot/json/HCAL_E_Max_Cell_truth_cut4.json"></plotlygraph>
</Transform>
</div>


---
layout: pageBar
---

# To-Do

<br>

- More statistic is done (5E8 inclusive). Running DAna with Acts Tracking.
- Determine rare process.
- A ([pyhf](https://pyhf.readthedocs.io/en/v0.7.6/)) workspace is made. Make Limit plot by MLE.
- Optimize cutflow (i.e. missingP > 7)

---
layout: center
class: "text-center"
hideInToc: true
---

# Thanks

[Git Repo](https://gitlab.com/ykrsama/darkshine-analysis)


---
layout: pageBar
---
