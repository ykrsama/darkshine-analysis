#!/usr/bin/env python3

"""
Plotting Variable distribution and cut-flow plot

Workflow:
- Read dataset list from txt file in ./datasets/
- Process root file using RDataframe, define columns and filters. Then save to miniTree.
- Plot histograms and cutflows
"""

from argparse import ArgumentParser
from pathlib import Path

import glob, os
import yaml
import shutil
import ROOT
import numpy as np
import plotly.graph_objs as go
from plotly.subplots import make_subplots
from plotly.offline import plot
from tools.plot import plot_style, add_histogram_data, add_histo1d, add_scatter, plot_histo2d, marker_symbol_cycle
from tools.gen_index import gen_index_html

INPUT_DIR = Path("datasets/positron")
OUTPUT_DIR = Path("output/job0")
LABEL=None
DATASETS = []
CUTFLOW = []
HISTOCAT = []
HISTO1DS = []
HISTO2DS = []
RESOLUTIONS = []
RES_TRUTHS = []
MINITREE_BRANCHES = {}
# MAPS
DATASETS_MAP, DATASETS_LABEL_MAP, DATASETS_ISSIG_MAP, DATASETS_USEWEIGHT_MAP, DATASETS_DASH_MAP = {}, {}, {}, {}, {}
FILTER_MAP, FILTER_LABEL_MAP = {}, {}
VAR_LABEL_MAP, VAR_BINNING_MAP, VAR_UNITCONVERT_MAP, VAR_CUTID_MAP, VAR_LOGY_MAP= {}, {}, {}, {}, {}
RES_DELTA_MAP, RES_TRUTH_MAP, RES_FILTER_MAP, RES_BINNING_MAP, RES_RELATIVE_MAP = {}, {}, {}, {}, {}
# Plot
PLOT_HEAD_MARGIN=0.3
JSON_LIST = []

def get_args():
    parser = ArgumentParser(description='Plotting script')
    parser.add_argument('-c', '--config', type=Path, default='share/config.yaml')
    parser.add_argument('-r', '--recreate', action='store_true', help='Re-create miniTree')
    parser.add_argument('-s', '--show', action='store_true', help='Show Figure')
    return parser.parse_args()


def get_value(data, key, default):
    if key in data:
        return data[key]
    return default


def read_config(yaml_file: Path):
    global INPUT_DIR, OUTPUT_DIR, DATASETS, CUTFLOW, LABEL, \
        HISTOCAT, HISTO1DS, HISTO2DS, RESOLUTIONS, MINITREE_BRANCHES  #,\
#        DATASETS_MAP, DATASETS_LABEL_MAP, DATASETS_ISSIG_MAP, DATASETS_USEWEIGHT_MAP, DATASETS_DASH_MAP, \
#        FILTER_MAP, FILTER_LABEL_MAP, \
#        VAR_LABEL_MAP, VAR_BINNING_MAP, VAR_UNITCONVERT_MAP, VAR_CUTID_MAP, VAR_LOGY_MAP, \
#        RES_DELTA_MAP, RES_TRUTH_MAP, RES_FILTER_MAP, RES_BINNING_MAP, RES_RELATIVE_MAP, RES_TRUTHS

    with open(yaml_file, 'r') as f:
        config       = yaml.safe_load(f)
        INPUT_DIR    = Path(get_value(config, 'Input', INPUT_DIR))
        OUTPUT_DIR   = Path(get_value(config, 'Output', OUTPUT_DIR))
        LABEL        = get_value(config, 'Label', LABEL)
        DATASETS     = get_value(config, 'Datasets', DATASETS)
        CUTFLOW      = get_value(config, 'CutFlow', CUTFLOW)
        HISTOCAT    += get_value(config, 'HistoCategorical', [])
        HISTO1DS    += get_value(config, 'Histo1Ds', [])
        HISTO2DS    += get_value(config, 'Histo2Ds', [])
        RESOLUTIONS += get_value(config, 'Resolutions', [])
        # Read Infos
        DATASETS_INFO = get_value(config, 'DatasetsInfo', {})
        FILTER_INFO = get_value(config, 'FilterInfo', {})
        VAR_INFO = get_value(config, 'VarInfo', {})
        RES_INFO = get_value(config, 'ResInfo', {})

    for key, info in DATASETS_INFO.items():
        DATASETS_MAP[key] = info["Dataset"]
        DATASETS_LABEL_MAP[key] = info["Label"]
        DATASETS_ISSIG_MAP[key] = info["IsSignal"]
        DATASETS_USEWEIGHT_MAP[key] = get_value(info, 'UseWeight', False)
        DATASETS_DASH_MAP[key] = get_value(info, 'Dash', None)

    for key, info in FILTER_INFO.items():
        FILTER_MAP[key] = info["Filter"]
        FILTER_LABEL_MAP[key] = info["Label"]

    for key, info in VAR_INFO.items():
        VAR_LABEL_MAP[key] = info["Label"]
        VAR_BINNING_MAP[key] = info["Binning"]
        VAR_UNITCONVERT_MAP[key] = eval(str(get_value(info, 'UnitConvert', 1)))
        VAR_CUTID_MAP[key] = get_value(info, "CutId", 0)
        VAR_LOGY_MAP[key] = get_value(info, 'Logy', True)
    
    for key, info in RES_INFO.items():
        RES_DELTA_MAP[key] = info["Delta"]
        RES_TRUTH_MAP[key] = info["Truth"]
        RES_FILTER_MAP[key] = get_value(info, 'Filter', 'None')
        RES_BINNING_MAP[key] = info["Binning"]
        RES_RELATIVE_MAP[key] = get_value(info, 'Relative', True)
    
    MINITREE_BRANCHES = set(["EventWeight"] + HISTO1DS + HISTOCAT + [x for xs in HISTO2DS for x in xs])


def declare_cxx_functions():
    ROOT.gInterpreter.Declare(r"""
    #ifndef DECLARE_CXX_FUNCTIONS
    #define DECLARE_CXX_FUNCTIONS

    #include "PhysicsProcessDef.h"

    struct PhysicsDef PDtable;

    // This func is used to filter the illed event in precut miniTree.
    Bool_t getSanity(ROOT::VecOps::RVec<int>& process_ID) {
        if( std::count(process_ID.begin(), process_ID.end(), 45) > 1 ) return false; // PositronDMBrem
        if( std::count(process_ID.begin(), process_ID.end(), 46) > 1 ) return false; // PositronAnnihilationTch
        if( std::count(process_ID.begin(), process_ID.end(), 47) > 1 ) return false; // PositronAnnihilationSch
        return true;
    }

    double getConvZ(ROOT::VecOps::RVec<int>& process_id, ROOT::VecOps::RVec<TVector3>& process_vertex) {
        for (int idx = 0; idx < process_id.size(); idx++) {
            if (process_vertex.at(idx).z() < -7.5 ) continue;
            if ( process_id.at(idx) == 5 ) {
                return process_vertex.at(idx).z();
            }
        }
        //return std::nan("RETURN");
        return -100;
    }

    TString getProcessOnTarget(ROOT::VecOps::RVec<int>& process_ID, ROOT::VecOps::RVec<TVector3>& process_vertex, ROOT::VecOps::RVec<Float_t>& process_energy, Double_t minProcE = 1000) {
        // sort by z
        std::vector<std::pair<int,double>> idx_z;
        for (int idx = 0; idx < process_vertex.size(); idx++) {
            idx_z.emplace_back(idx, process_vertex.at(idx).z());
        }
        std::sort(idx_z.begin(), idx_z.end(), [](const auto& l, const auto& r){ return l.second < r.second; });
        TString ret_name = "";
        TString prev_name = "";
        TString cur_name = "";
        int same_proc_count = 1;
        // for (int id = 0; id < process_ID.size(); id++) {
        for (const auto cur_idx_z : idx_z) {
            int idx = cur_idx_z.first;
            // int idx = idx_z.at(id).first;
            if (process_energy.at(idx) < minProcE) continue;
            if (process_vertex.at(idx).z() > 7.5 ) continue;
            int procId = process_ID.at(idx);
            if (procId >= 0 && procId < PDtable.dPhyTypeVec.size()) {
                cur_name = PDtable.dPhyTypeVec.at(process_ID.at(idx));
            } else {
                cur_name = "Unknown";
            }
            if (ret_name != "") {
                if (prev_name == cur_name) {
                    same_proc_count++;
                    if (same_proc_count > 2) {
                        int pos = ret_name.Last('x');
                        ret_name = ret_name.Remove(pos - 1);
                    }
                    ret_name += TString(" x ") + TString::Itoa(same_proc_count, 10);
                } else {
                    ret_name += " + ";
                    ret_name += cur_name;
                    same_proc_count = 1;
                }
            } else {
                ret_name += cur_name;
            }
            prev_name = cur_name;
        }
        if (ret_name == "") ret_name = "None";
        return ret_name;
    }
    
    int getMaxEnergyProcessIndex(ROOT::VecOps::RVec<Float_t>& process_energy, ROOT::VecOps::RVec<TVector3>& process_vertex) {
        for (int i = 0; i < process_energy.size(); i++) {
            // if (process_PVName.at(i) == PVName) continue;
            if (process_vertex.at(i).z() > -7.5 && process_vertex.at(i).z() < 7.5) continue;
            process_energy.at(i) = -1;
        }
        auto result = std::max_element(process_energy.begin(), process_energy.end());
        if (result == process_energy.end())
            return -1;
        if (*result < 0)
            return -1;
        return (int)std::distance(process_energy.begin(), result);
    }
    
    int getMaxProcessID(ROOT::VecOps::RVec<int>& process_id, int idx) {
        if (idx < 0 || process_id.empty()) return -1;
        return process_id.at(idx);
    }
    
    Float_t getMaxProcessEnergy(ROOT::VecOps::RVec<Float_t>& process_energy, int idx) {
        if (idx < 0 || process_energy.empty()) return -1;
        return process_energy.at(idx);
    }
    
    TString getProcessName(int process_id) {
        if (process_id >= PDtable.dPhyTypeVec.size())
            return TString::Itoa(process_id, 10);
        if (! (process_id >= 0))
            return "None";
        return PDtable.dPhyTypeVec.at(process_id);
    }
    
    Double_t getMissingP(ROOT::VecOps::RVec<Double_t>& TagTrk2_pp, ROOT::VecOps::RVec<Double_t>& RecTrk2_pp) {
        if (TagTrk2_pp.empty()) {
            if (RecTrk2_pp.empty())
                return std::nan("RETURN");
            return -RecTrk2_pp.at(0);
        }
        if (RecTrk2_pp.empty()) {
            return TagTrk2_pp.at(0);
        }
        return TagTrk2_pp.at(0) - RecTrk2_pp.at(0);
    }
    
    Double_t getMissingE(ROOT::VecOps::RVec<Double_t>& TagTrk2_pp, ROOT::VecOps::RVec<Double_t>& ECAL_E_total) {
        if (TagTrk2_pp.empty())
            return std::nan("RETURN");
            //return -9999;
        return TagTrk2_pp.at(0) - ECAL_E_total.at(1);
    
    }

    Double_t getCosTheta(const ROOT::VecOps::RVec<Double_t>& p) {
        assert(p.size()>=3);
        return p.at(2) / sqrt(p.at(0) * p.at(0) + p.at(2) * p.at(2));
    }

    std::array<Double_t, 4> Boost(const std::array<Double_t, 4>& p, const std::array<Double_t, 3>& beta) {
        Double_t b2 = beta.at(0) * beta.at(0) + beta.at(1) * beta.at(1) + beta.at(2) * beta.at(2);
        Double_t gamma = 1 / sqrt(1 - b2);
        Double_t bp = p.at(0) * beta.at(0) + p.at(1) * beta.at(1) + p.at(2) * beta.at(2);
        Double_t gamma2 = b2 > 0 ? (gamma - 1) / b2 : 0.0;

        std::array<Double_t, 4> pout = {
            p.at(0) + gamma2 * bp * beta.at(0) + gamma * beta.at(0) * p.at(3),
            p.at(1) + gamma2 * bp * beta.at(1) + gamma * beta.at(1) * p.at(3),
            p.at(2) + gamma2 * bp * beta.at(2) + gamma * beta.at(2) * p.at(3),
            gamma * (p.at(3) + bp)
        };
        return pout;
    }

    std::array<Double_t, 3> getBeta(const std::array<Double_t, 4>& p) {
        Double_t e = p.at(3);
        if (e == 0) return {0, 0, 0};
        return {p.at(0) / e, p.at(1) / e, p.at(2) / e};
    }

    std::array<Double_t, 3> getMinusBeta(const std::array<Double_t, 4>& p) {
        Double_t e = p.at(3);
        if (e == 0) return {0, 0, 0};
        return {-p.at(0) / e, -p.at(1) / e, -p.at(2) / e};
    }


    Double_t getCosTheta_COM(const ROOT::VecOps::RVec<Double_t>& p1, Double_t e1, const ROOT::VecOps::RVec<Double_t>& p2, Double_t e2) {
        assert(p1.size()>=3 && p2.size()>=3);
        std::array<Double_t, 4> p12 = {p1.at(0) + p2.at(0), p1.at(1) + p2.at(1), p1.at(2) + p2.at(2), e1 + e2};
        auto _beta = getMinusBeta(p12);
        std::array<Double_t, 4> part1 = {p1.at(0), p1.at(1), p1.at(2), e1};
        auto bp1 = Boost(part1, _beta);
        // get direction of COM momentum
        auto beta_norm = sqrt(_beta.at(0) * _beta.at(0) + _beta.at(1) * _beta.at(1) + _beta.at(2) * _beta.at(2));
        std::array<Double_t, 3> beta_unit = { _beta.at(0) / beta_norm, _beta.at(1) / beta_norm, _beta.at(2) / beta_norm };
        auto bp1_P = sqrt(bp1.at(0) * bp1.at(0) + bp1.at(1) * bp1.at(1) + bp1.at(2) * bp1.at(2));
        if (bp1_P == 0) return std::nan("RETURN");
        // project bp1 on beta_unit
        auto cos_theta = (bp1.at(0) * beta_unit.at(0) + bp1.at(1) * beta_unit.at(1) + bp1.at(2) * beta_unit.at(2)) / bp1_P;
        return cos_theta;
    }

    // There are no momentum conservation in fixed-target experiment.
    // Currently using an approximation of boost to center-of-mass frame for e+ e- collision
    // Todo: wait ntuple to add precise incoming beam four-momentum
    Double_t getCosTheta_boost_z(const ROOT::VecOps::RVec<Double_t>& p3, Double_t e3, Double_t TagTrk2_pp_truth_fin) {
        assert(p3.size()>=3);
        //std::array<Double_t, 4> p1 = {0, 0, TagTrk2_pp_truth_fin, sqrt(TagTrk2_pp_truth_fin * TagTrk2_pp_truth_fin + 0.511 * 0.511)};
        //std::array<Double_t, 4> p2 = {0, 0, 0, 0.511};
        //std::array<Double_t, 4> p12 = {p1.at(0) + p2.at(0), p1.at(1) + p2.at(1), p1.at(2) + p2.at(2), p1.at(3) + p2.at(3)};
        //auto _beta = getMinusBeta(p12);
        //std::array<Double_t, 4> part3 = {p3.at(0), p3.at(1), p3.at(2), e3};
        //auto bp3 = Boost(part3, _beta);
        std::array<Double_t, 3> bp3 = {p3.at(0), p3.at(1), p3.at(2) - 0.5 * TagTrk2_pp_truth_fin}; // verry rough approximation
        auto bp3_P = sqrt(bp3.at(0) * bp3.at(0) + bp3.at(1) * bp3.at(1) + bp3.at(2) * bp3.at(2));
        if (bp3_P == 0) return std::nan("RETURN"); // return -9999;
        return bp3.at(2) / bp3_P; // Cos theta
    }

    Double_t getTheta_boost_z(const ROOT::VecOps::RVec<Double_t>& p3, Double_t e3, Double_t TagTrk2_pp_truth_fin) {
        assert(p3.size()>=3);
        std::array<Double_t, 3> bp3 = {p3.at(0), p3.at(1), p3.at(2) - 0.5 * TagTrk2_pp_truth_fin}; // verry rough approximation
        auto bp3_P = sqrt(bp3.at(0) * bp3.at(0) + bp3.at(1) * bp3.at(1) + bp3.at(2) * bp3.at(2));
        if (bp3_P == 0) return std::nan("RETURN"); // return -9999;
        return acos(bp3.at(2) / bp3_P); // theta
    }


    Double_t dummy() {
        return std::nan("RETURN");
        //return -9999;
    }

    // Acts Tracks
    Double_t get_dTrk_P_n(Double_t n_gev, const ROOT::VecOps::RVec<Double_t>& rec, Double_t truth) {
        if (rec.empty())
            return std::nan("RETURN");
        double pmin = (n_gev - 0.5) * 1000;
        double pmax = (n_gev + 0.5) * 1000;
        if (! (truth > pmin && truth < pmax) )
            return std::nan("RETURN");
        return rec.front() - truth;
    }

    #endif // DECLARE_CXX_FUNCTIONS
    """)


def add_rdf_column(rdf, is_sig: bool, use_weight: bool):
    declare_cxx_functions()
#    rdf = rdf.Define("sanity", "getSanity(process_ID)").Filter("sanity==true");
#    if is_sig:
#        rdf = (rdf
#              .Define("EAAp", "Truth_DP_E + Truth_Gam_E")
#              .Define("DP_cosTheta", "getCosTheta(Truth_DP_P)")
#              .Define("Gam_cosTheta", "getCosTheta(Truth_Gam_P)")
#              .Define("DP_cosTheta_COM","getCosTheta_COM(Truth_DP_P, Truth_DP_E, Truth_Gam_P, Truth_Gam_E)" )
#              .Define("Gam_cosTheta_COM", "getCosTheta_COM(Truth_Gam_P, Truth_Gam_E, Truth_DP_P, Truth_DP_E)")
#              .Define("AutoRecoilTheta", "acos(getCosTheta(Truth_Gam_P))")
#              .Define("DP_Theta", "acos(getCosTheta(Truth_DP_P))")
#              )
#    else:
#        rdf = (rdf
#              .Define("Truth_Gam_E", "dummy()")
#              .Define("EAAp", "dummy()")
#              .Define("DP_cosTheta", "dummy()")
#              .Define("Gam_cosTheta", "dummy()")
#              .Define("DP_cosTheta_COM","dummy()" )
#              .Define("Gam_cosTheta_COM", "dummy()")
#              .Define("AutoRecoilTheta", "Target_Recoil_theta")
#              .Define("DP_Theta", "dummy()")
#              #.Define("Truth_DP_E", "dummy()")
#              )
    return (rdf
            .Define("EventWeight", "(mcEventWeight > 0 && mcEventWeight < 100) ? mcEventWeight : 0.0" if use_weight else "1.0")
            # MC Truth
            #.Define("maxEIndex", r"""getMaxEnergyProcessIndex(process_energy, process_vertex)""")
            #.Define("maxProcID", "getMaxProcessID(process_ID, maxEIndex)")
            #.Define("maxProcName", "getProcessName(maxProcID)")
            #.Define("maxProcE", "getMaxProcessEnergy(process_energy, maxEIndex)")
            .Define("ProcessOnTarget", """getProcessOnTarget(process_ID, process_vertex, process_energy)""")
            .Define("ECAL_E_max_truth", r'ECAL_E_max.empty() ? std::nan("RETURN") : ECAL_E_max.at(0)')
            .Define("convZ", "getConvZ(process_ID, process_vertex)")
            # Detector Truth
            .Define("ECAL_seed_truth_n", "ECal_seed_e_truth.size()")
            .Define("ECAL_E_truth", r'ECAL_E_total.empty() ? std::nan("RETURN") : ECAL_E_total.at(0)')
            .Define("HCAL_E_truth", r'HCAL_E_total.empty() ? std::nan("RETURN") : HCAL_E_total.at(0)')
            .Define("HCAL_E_Max_Cell_truth", r'HCAL_E_Max_Cell.empty() ? std::nan("RETURN") : HCAL_E_Max_Cell.at(0)')
            .Define("SideHCAL_E_truth", r'SideHCAL_E_total.empty() ? std::nan("RETURN") : SideHCAL_E_total.at(0)')
            .Define("SideHCAL_E_Max_Cell_truth", r'SideHCAL_E_Max_Cell.empty() ? std::nan("RETURN") : SideHCAL_E_Max_Cell.at(0)')
            .Define("ECAL_E_max_smear", r'ECAL_E_max.empty() ? std::nan("RETURN") : ECAL_E_max.at(1)')
            # Digitized
            .Define("MissingP", "getMissingP(TagTrk2_pp, RecTrk2_pp)")
            .Define("MissingE", "getMissingE(TagTrk2_pp, ECAL_E_total)")
            .Define("ECAL_E_smear", r'ECAL_E_total.empty() ? std::nan("RETURN") : ECAL_E_total.at(1)')
            # Reconstructed
            .Define("TagTrk2_track_No_", "TagTrk2_track_No >=0 ? TagTrk2_track_No : 0")
            .Define("RecTrk2_track_No_", "RecTrk2_track_No >=0 ? RecTrk2_track_No : 0")
            .Define("TagTrk2_pp_", r'TagTrk2_track_No > 0 ? TagTrk2_pp.at(0) : std::nan("RETURN")')
            .Define("RecTrk2_pp_", r'RecTrk2_track_No > 0 ? RecTrk2_pp.at(0) : std::nan("RETURN")')
            .Define("ECAL_Cluster_X0", r"""ECAL_Cluster_X.empty() ? std::nan("RETURN") : ECAL_Cluster_X.at(0)""")
            .Define("ECAL_Cluster_N_RecTrk_N", "ECAL_Cluster_N - RecTrk2_track_No")
            .Define("dTagTrk2_pp", r'TagTrk2_pp_ - TagTrk2_pp_truth_ini' )
            .Define("dRecTrk2_pp", r'RecTrk2_pp_ - RecTrk2_pp_truth_ini' )
#            # Performance
#            .Define("dTagTrk2_pp_8", "get_dTrk_P_n(8, TagTrk2_pp, TagTrk2_pp_truth_ini)")
#            .Define("dRecTrk2_pp_1", "get_dTrk_P_n(1, RecTrk2_pp, RecTrk2_pp_truth_ini)")
#            .Define("dRecTrk2_pp_2", "get_dTrk_P_n(2, RecTrk2_pp, RecTrk2_pp_truth_ini)")
#            .Define("dRecTrk2_pp_3", "get_dTrk_P_n(3, RecTrk2_pp, RecTrk2_pp_truth_ini)")
            .Define("dRecTrk2_pp_4", "get_dTrk_P_n(4, RecTrk2_pp, RecTrk2_pp_truth_ini)")
#            .Define("dRecTrk2_pp_5", "get_dTrk_P_n(5, RecTrk2_pp, RecTrk2_pp_truth_ini)")
#            .Define("dRecTrk2_pp_6", "get_dTrk_P_n(6, RecTrk2_pp, RecTrk2_pp_truth_ini)")
#            .Define("dRecTrk2_pp_7", "get_dTrk_P_n(7, RecTrk2_pp, RecTrk2_pp_truth_ini)")
#            .Define("dRecTrk2_pp_8", "get_dTrk_P_n(8, RecTrk2_pp, RecTrk2_pp_truth_ini)")
            .Define("Acts_TagTrk_P_", r'Acts_TagTrk_No > 0 ? std::abs(Acts_TagTrk_P.at(0)) : std::nan("RETURN")')
            .Define("Acts_RecTrk_P_", r'Acts_RecTrk_No > 0 ? std::abs(Acts_RecTrk_P.at(0)) : std::nan("RETURN")')
            .Define("Acts_MissingP", r'std::abs(getMissingP(Acts_TagTrk_P, Acts_RecTrk_P))')
            .Define("dActs_TagTrk_P", r'Acts_TagTrk_No > 0 ? std::abs(Acts_TagTrk_P.at(0)) - TagTrk2_pp_truth_ini : std::nan("RETURN")')
            .Define("dActs_RecTrk_P", r'Acts_RecTrk_No > 0 ? std::abs(Acts_RecTrk_P.at(0)) - RecTrk2_pp_truth_ini : std::nan("RETURN")')
#            .Define("dActs_TagTrk_P_8", "get_dTrk_P_n(8, Acts_TagTrk_P, TagTrk2_pp_truth_ini)")
#            .Define("dActs_RecTrk_P_1", "get_dTrk_P_n(1, Acts_RecTrk_P, RecTrk2_pp_truth_ini)")
#            .Define("dActs_RecTrk_P_2", "get_dTrk_P_n(2, Acts_RecTrk_P, RecTrk2_pp_truth_ini)")
#            .Define("dActs_RecTrk_P_3", "get_dTrk_P_n(3, Acts_RecTrk_P, RecTrk2_pp_truth_ini)")
            .Define("dActs_RecTrk_P_4", "get_dTrk_P_n(4, Acts_RecTrk_P, RecTrk2_pp_truth_ini)")
#            .Define("dActs_RecTrk_P_5", "get_dTrk_P_n(5, Acts_RecTrk_P, RecTrk2_pp_truth_ini)")
#            .Define("dActs_RecTrk_P_6", "get_dTrk_P_n(6, Acts_RecTrk_P, RecTrk2_pp_truth_ini)")
#            .Define("dActs_RecTrk_P_7", "get_dTrk_P_n(7, Acts_RecTrk_P, RecTrk2_pp_truth_ini)")
#            .Define("dActs_RecTrk_P_8", "get_dTrk_P_n(8, Acts_RecTrk_P, RecTrk2_pp_truth_ini)")
#            # Acts ECAL Seed
#            .Define("ECal_seed_x_truth_0", r'ECal_seed_x_truth.empty() ? std::nan("RETURN"): ECal_seed_x_truth.at(0)')
#            .Define("ECal_seed_y_truth_0", r'ECal_seed_y_truth.empty() ? std::nan("RETURN"): ECal_seed_y_truth.at(0)')
#            .Define("ECal_seed_px_truth_0", r'ECal_seed_px_truth.empty() ? std::nan("RETURN"): ECal_seed_px_truth.at(0)')
#            .Define("ECal_seed_py_truth_0", r'ECal_seed_py_truth.empty() ? std::nan("RETURN"): ECal_seed_py_truth.at(0)')
#            .Define("ECal_seed_pz_truth_0", r'ECal_seed_pz_truth.empty() ? std::nan("RETURN"): ECal_seed_pz_truth.at(0)')
#            .Define("Acts_ECal_seed_x_0", r'Acts_ECal_seed_x.empty() ? std::nan("RETURN"): Acts_ECal_seed_x.at(0)')
#            .Define("Acts_ECal_seed_y_0", r'Acts_ECal_seed_y.empty() ? std::nan("RETURN"): Acts_ECal_seed_y.at(0)')
#            .Define("Acts_ECal_seed_px_0", r'Acts_ECal_seed_px.empty() ? std::nan("RETURN"): Acts_ECal_seed_px.at(0)')
#            .Define("Acts_ECal_seed_py_0", r'Acts_ECal_seed_py.empty() ? std::nan("RETURN"): Acts_ECal_seed_py.at(0)')
#            .Define("Acts_ECal_seed_pz_0", r'Acts_ECal_seed_pz.empty() ? std::nan("RETURN"): Acts_ECal_seed_pz.at(0)')
#            .Define("dActs_ECal_seed_x",  r'ECal_seed_x_truth.empty() || Acts_ECal_seed_x.empty() ? std::nan("RETURN") : Acts_ECal_seed_x_0  - ECal_seed_x_truth_0')
#            .Define("dActs_ECal_seed_y",  r'ECal_seed_y_truth.empty() || Acts_ECal_seed_y.empty() ? std::nan("RETURN") : Acts_ECal_seed_y_0  - ECal_seed_y_truth_0')
#            .Define("dActs_ECal_seed_px", r'ECal_seed_px_truth.empty() || Acts_ECal_seed_px.empty() ? std::nan("RETURN") : Acts_ECal_seed_px_0 - ECal_seed_px_truth_0')
#            .Define("dActs_ECal_seed_py", r'ECal_seed_py_truth.empty() || Acts_ECal_seed_py.empty() ? std::nan("RETURN") : Acts_ECal_seed_py_0 - ECal_seed_py_truth_0')
#            .Define("dActs_ECal_seed_pz", r'ECal_seed_pz_truth.empty() || Acts_ECal_seed_pz.empty() ? std::nan("RETURN") : Acts_ECal_seed_pz_0 - ECal_seed_pz_truth_0')
            )



def read_dataset_list():
    dataset_file_map = {}
    for dataset in DATASETS:
        list_file = INPUT_DIR / DATASETS_MAP[dataset]
        dana_files = []
        assert list_file.exists(), f"{list_file} not found"
        with open(str(list_file), 'r') as f:
            for line in f:
                if line.startswith('#'):
                    continue
                if not Path(line.strip()).exists():
                    print(f"[Warning] {line.strip()} does not exist.", flush=True)
                    continue
                dana_files.append(line.strip())
        dataset_file_map[dataset] = dana_files
    return dataset_file_map


def fill_cutflow(hist, report, total_weight_pass):
    hist.GetXaxis().SetBinLabel(1, "w/o cut")
    """HCAL E<sub>Mac Cell</sub> < 5 MeV: pass=317        all=317        -- eff=100.00 % cumulative eff=0.32 %"""
    # Fill cutflow histogram
    hist.AddBinContent(1, total_weight_pass[0])
    for i, cut in enumerate(report, start=2):
        weight_pass = total_weight_pass[i-1]  # cut.GetPass()
        weight_all = total_weight_pass[i-2]
        eff=100 * weight_pass / weight_all if weight_all > 0 else 0
        cumulate_eff = 100 * weight_pass / total_weight_pass[0] if total_weight_pass[0] > 0 else 0
        hist.GetXaxis().SetBinLabel(i, cut.GetName())
        hist.AddBinContent(i, weight_pass)
        print(f"{cut.GetName()}: pass={round(weight_pass,2)}\tall={round(weight_all,2)}\t-- eff={round(eff,2)} % cumulate eff={round(cumulate_eff,2)} %")
    return hist


def fill_mini_tree(dana_file: str, dataset_dir: Path, mini_tree_name: str, dataset: str):
    # Create directories
    precut_dir = dataset_dir / "precut"
    precut_dir.mkdir(parents=True, exist_ok=True)
    # Read DAna root file
    rdf = ROOT.RDataFrame("dp", dana_file)
    rdf = add_rdf_column(rdf, DATASETS_ISSIG_MAP[dataset], DATASETS_USEWEIGHT_MAP[dataset])
    #precut_count = rdf.Count()
    total_weight_pass = [rdf.Sum("EventWeight").GetValue()]  # 0th is original
    # Check if all columns exists
    for col in MINITREE_BRANCHES:
        if not rdf.HasColumn(col):
            print(f"[ERROR] {col} does not exist. Check your config.", flush=True)
            exit()
    # Snapshot pre-cut file
    precut_miniTree_file = f"{precut_dir}/{mini_tree_name}"
    rdf.Snapshot("dp", precut_miniTree_file, MINITREE_BRANCHES)
    # Perform cut-flow
    for i, cut in enumerate(CUTFLOW, 1):
        if cut in FILTER_MAP:
            rdf = rdf.Filter(FILTER_MAP[cut], FILTER_LABEL_MAP[cut])
        else:
            rdf = rdf.Filter(cut, cut)
        total_weight_pass.append(rdf.Sum("EventWeight").GetValue())
        postcut_dir = dataset_dir / f"cut{i}"
        postcut_dir.mkdir(parents=True, exist_ok=True)
        postcut_miniTree_file = f"{postcut_dir}/{mini_tree_name}"
        if rdf.Count().GetValue() == 0:
            # print(f"[INFO] {cut} cut has no event left.")
            continue
        rdf.Snapshot("dp", postcut_miniTree_file, MINITREE_BRANCHES)
    report = rdf.Report()
    del rdf
    return report, total_weight_pass


def process_dataset():
    # Remove old mini_tree_dir if this directory already exists
    if mini_tree_dir.exists():
        rm_exit = input(f"[INFO] miniTree directory {mini_tree_dir} already exists. Do you want to remove it? (y/[n]): ")
        if rm_exit.lower() != 'y':
            print(f"[INFO] Skip processing dataset. Exiting...", flush=True)
            exit()
        print(f"[INFO] Removing existing miniTree directory {mini_tree_dir}", flush=True)
        shutil.rmtree(mini_tree_dir)
    dataset_file_map = read_dataset_list()
    print(f"[INFO] miniTree Columns: {MINITREE_BRANCHES}", flush=True)
    # Process DAna files and get cutflow report
    for dataset in DATASETS:
        dataset_dir = mini_tree_dir / dataset
        # Create a TH1D histogram
        n_cut = len(CUTFLOW) + 1
        cutflow_hist = ROOT.TH1D(f"cutflow", f"Cutflow {dataset}", n_cut, 0, n_cut)
        for i, dana_file in enumerate(dataset_file_map[dataset]):
            print(f"-- Processing {dana_file}", flush=True)
            mini_tree_name = f"miniTree{i}.root"
            #report, precut_count = fill_mini_tree(dana_file, dataset_dir, mini_tree_name, dataset)
            report, total_weight_pass = fill_mini_tree(dana_file, dataset_dir, mini_tree_name, dataset)
            #report.Print()
            #cutflow_hist = fill_cutflow(cutflow_hist, report, precut_count)
            cutflow_hist = fill_cutflow(cutflow_hist, report, total_weight_pass)
        # Save cutflow histogram to a root file
        output_file = ROOT.TFile(f"{dataset_dir}/cutflow.root", "RECREATE")
        cutflow_hist.Write()
        output_file.Close()
        del cutflow_hist


def write_fig(fig, filename):
    global JSON_LIST
    filepath=f"{OUTPUT_DIR}/plot/<format>/{filename}.<format>"
    JSON_LIST.append(f"{filename}.json")
    for out_format in ['json', 'pdf', 'png']:
        (OUTPUT_DIR / "plot" / out_format).mkdir(parents=True, exist_ok=True)
        fig.write_image(filepath.replace('<format>', out_format), scale=2 if out_format == 'png' else 1)

    # HTML
    plotly_config = {
        'toImageButtonOptions': {
            'format': 'png',  # Change to 'svg' here
            'scale': 2,  # Increase scale in case of high-resolution images
            'filename': 'custom_image',
        }
    }
    (OUTPUT_DIR / "plot/html").mkdir(parents=True, exist_ok=True)
    plotly_config['toImageButtonOptions']['filename'] = filename
    plot(fig, config=plotly_config, filename=filepath.replace('<format>','html'), auto_open=False, include_mathjax = 'cdn')


def plot_histoCategorical():
    print(f"-- Plotting {varname}", flush=True)
    varlabel = varname
    varcutid = 0
    if varname in VAR_LABEL_MAP:
        varlabel = VAR_LABEL_MAP[varname]
        varcutid = VAR_CUTID_MAP[varname]

    cutflow_label = get_cutflow_label(varcutid)
    cutflow_subdir = 'precut' if varcutid == 0 else f"cut{varcutid}"

    xtickangle = 0
    fig_title = ''
    xtitle = varlabel
    yrange = [-6, 0 + 6 * PLOT_HEAD_MARGIN]
    fig = go.Figure()
    for dataset in DATASETS:
        dataset_dir = mini_tree_dir / dataset
        if len(glob.glob(f"{dataset_dir}/{cutflow_subdir}/miniTree*.root")) == 0:
            print(f"[Warning] No miniTree is found for {dataset} in {cutflow_subdir}. Skip plotting.", flush=True)
            continue
        df = ROOT.RDataFrame("dp", f"{dataset_dir}/{cutflow_subdir}/miniTree*.root").AsNumpy([varname])
        if type(df[varname][0]).__name__ == 'TString':
            datax = [str(item) for item in df[varname]]
            xtickangle = 30
            yrange = [-6, 1.5]
            fig_title = varlabel
            xtitle = ''
        else:
            datax = df[varname]
        fig.add_trace(
            go.Histogram(
                x=datax,
                histnorm='probability density',
                name=DATASETS_LABEL_MAP[dataset],
                marker_color='#7F7F7F' if dataset == 'inclusive' else None
            )
        )
    plot_style(
        fig,
        fig_width=900,
        fig_height=675,
        fig_title=fig_title,
        xaxes_title=xtitle,
        yaxes_title='Events Normalized',
        yaxes_type='log',
        xaxes_tickangle=xtickangle,
        yaxes_range=yrange,
        darkshine_label3=LABEL if LABEL else cutflow_label,
    )
    fig.update_layout(
        xaxis=dict(
            categoryorder='total descending'
        )
    )
    if show_fig:
        fig.show()
    write_fig(fig, f"{varname}_{cutflow_subdir}")


def update_binning_range(binning: str, unitconv: float):
    # input binning: bin centers
    # output binning: left edges
    nbin = eval(str(binning[0]))
    minbin = eval(str(binning[1]))
    maxbin = eval(str(binning[2]))
    dx = (maxbin - minbin) / nbin
    nbin += 1
    # extend the binning range
    newbinning = [nbin + 2*nbin, minbin - 0.5*dx - nbin*dx, maxbin + 0.5*dx + nbin*dx]
    xrange = [(minbin - 0.5*dx) * unitconv, (maxbin + 0.5*dx) * unitconv]
    return newbinning, xrange


def get_cutflow_label(cut_index: int):
    assert cut_index <= len(CUTFLOW), f"CutId {cut_index} is out of range. Length of cutflow is {len(CUTFLOW)}."
    if cut_index < 1:
        return 'w/o cut'
    cut = CUTFLOW[0]
    cutflow_label = FILTER_LABEL_MAP[cut] if cut in FILTER_LABEL_MAP else cut
    for cut in CUTFLOW[1:cut_index]:
        cut_label = FILTER_LABEL_MAP[cut] if cut in FILTER_LABEL_MAP else cut
        cutflow_label += ', ' + cut_label
    return cutflow_label


def plot_histo1D():
    print(f"-- Plotting {varname}", flush=True)
    varlabel = varname
    varbinning = None
    xrange = None
    varunitconv = 1
    varcutid = 0
    varlogy = True
    if varname in VAR_LABEL_MAP:
        varlabel = VAR_LABEL_MAP[varname]
        varbinning, xrange = update_binning_range(VAR_BINNING_MAP[varname], VAR_UNITCONVERT_MAP[varname])
        varunitconv = VAR_UNITCONVERT_MAP[varname]
        varcutid = VAR_CUTID_MAP[varname]
        varlogy = VAR_LOGY_MAP[varname]

    cutflow_label = get_cutflow_label(varcutid)
    cutflow_subdir = 'precut' if varcutid == 0 else f"cut{varcutid}"

    fig = go.Figure()
    ymins=np.array([])
    ymaxs=np.array([])
    for dataset in DATASETS:
        dataset_dir = mini_tree_dir / dataset
        if len(glob.glob(f"{dataset_dir}/{cutflow_subdir}/miniTree*.root")) == 0:
            print(f"[Warning] No miniTree is found for {dataset} in {cutflow_subdir}. Skip plotting.", flush=True)
            continue
        # Plot Histo1Ds
        rdf = ROOT.RDataFrame("dp", f"{dataset_dir}/{cutflow_subdir}/miniTree*.root")
        if varbinning:
            histo = rdf.Histo1D((varname, varname, *varbinning), varname, "EventWeight")
        else:
            histo = rdf.Histo1D(varname, "EventWeight")
        if histo.Integral() > 0:
            #histo.Scale(1/histo.Integral("width")/varunitconv)
            histo.Scale(1/histo.Integral())
        ds_label = DATASETS_LABEL_MAP[dataset]
        # Get FWHM
        if "ΔP" in varlabel:
            bin1 = histo.FindFirstBinAbove(histo.GetMaximum()/2)
            bin2 = histo.FindLastBinAbove(histo.GetMaximum()/2)
            fwhm = (histo.GetBinCenter(bin2) - histo.GetBinCenter(bin1)) * 0.001
            ds_label = ds_label + f" FWHM={round(fwhm,2)} GeV"
            print(fwhm, flush=True)
        valuemin, valuemax = add_histo1d(
            fig,
            histo,
            name=ds_label,
            unitconv=varunitconv,
            #color='#7F7F7F' if dataset == 'inclusive' else None,
            dash=DATASETS_DASH_MAP[dataset],
        )
        if valuemin and valuemax:
            ymins = np.append(ymins, valuemin)
            ymaxs = np.append(ymaxs, valuemax)
    if ymins.size == 0:
        print(f"[Warning] No histogram is found for {varname}. Skip plotting.", flush=True)
        return
    if varlogy:
        if ymins.min() > 0:
            log_dy = np.log10(ymaxs.max()) - np.log10(ymins.min())
            new_yrange=[np.log10(ymins.min()), np.log10(ymaxs.max()) + log_dy * PLOT_HEAD_MARGIN]
        else:
            new_yrange = [np.log10(ymaxs.max()) - 5, np.log10(ymaxs.max()) + 5 * PLOT_HEAD_MARGIN]
    else:
        dy = ymaxs.max() - ymins.min()
        new_yrange=[ymins.min(), ymaxs.max() + dy * PLOT_HEAD_MARGIN]
    plot_style(
        fig,
        fig_width=600 if len(DATASETS) < 4 else 700,
        fig_height=600,
        fig_title='',
        xaxes_title=varlabel,
        yaxes_title='Events Normalized',
        yaxes_type='log' if varlogy else None,
        xaxes_range=xrange,
        yaxes_range=new_yrange,
        darkshine_label3=LABEL if LABEL else cutflow_label
    )
    if show_fig:
        fig.show()
    write_fig(fig, f"{varname}_{cutflow_subdir}")


def plot_histo2Ds():
    print(f"-- Plotting {varname}", flush=True)
    varlabel = [varname[0], varname[1]]
    varbinning = [None, None]
    xrange = [None, None]
    varunitconv = [1, 1]
    varcutid = 0
    for i in [0, 1]:
        # if varname[i] in VAR_LABEL_MAP
        assert varname[i] in VAR_LABEL_MAP 
        varlabel[i] = VAR_LABEL_MAP[varname[i]]
        varbinning[i], xrange[i] = update_binning_range(VAR_BINNING_MAP[varname[i]], VAR_UNITCONVERT_MAP[varname[i]])
        varunitconv[i] = VAR_UNITCONVERT_MAP[varname[i]]
        if VAR_CUTID_MAP[varname[i]] > varcutid:
            varcutid = VAR_CUTID_MAP[varname[i]]

    cutflow_label = get_cutflow_label(varcutid)
    cutflow_subdir = 'precut' if varcutid == 0 else f"cut{varcutid}"

    row_n = int(np.floor(np.sqrt(len(DATASETS))))
    col_n = int(np.ceil(np.sqrt(len(DATASETS))))
    if (row_n * col_n) < len(DATASETS):
        row_n += 1
    subplot_titles=[]
    for dataset in DATASETS:
        subplot_titles.append(DATASETS_LABEL_MAP[dataset])
    fig = make_subplots(
            rows=row_n, cols=col_n,
            shared_xaxes=True, shared_yaxes=True,
            vertical_spacing=0.04, horizontal_spacing=0.04,
            subplot_titles=subplot_titles
    )

    for fig_id, dataset in enumerate(DATASETS):
        # Subplot position
        row_id = fig_id // col_n + 1
        col_id = fig_id % col_n + 1
        # Read dataset
        dataset_dir = mini_tree_dir / dataset
        if len(glob.glob(f"{dataset_dir}/{cutflow_subdir}/miniTree*.root")) == 0:
            print(f"[Warning] No miniTree is found for {dataset} in {cutflow_subdir}. Skip plotting.", flush=True)
            continue
        rdf = ROOT.RDataFrame("dp", f"{dataset_dir}/{cutflow_subdir}/miniTree*.root")
        histo = rdf.Histo2D((f"{varname[0]}_vs_{varname[1]}", f"{varname[0]}_vs_{varname[1]}", *(varbinning[0]), *(varbinning[1])), varname[0], varname[1], "EventWeight")
        fig.add_trace(
            plot_histo2d(
                histo,
                name=DATASETS_LABEL_MAP[dataset],
                unitconv=varunitconv,
                normalize_mode=None
            ),
            row=row_id, col=col_id,
        )

    plot_style(
        fig,
        fig_width=400*col_n,
        fig_height=400*row_n*0.95,
        xaxes_range=xrange[0],
        yaxes_range=xrange[1],
        darkshine_label3=LABEL if LABEL else cutflow_label,
    )
    fig.update_layout(
        margin=dict(b=65),
        annotations=list(fig.layout.annotations) + [
            dict(
                text=varlabel[0],  # x-axis label
                showarrow=False,
                xref="paper",  # position relative to the whole paper
                yref="paper",
                x=0.5,  # position in the middle of the x-axis
                y=-0.1,  # position below the x-axis
                xanchor="center",
                yanchor="bottom",
                font=dict(size=20)
            ),
            dict(
                text=varlabel[1],  # y-axis label
                showarrow=False,
                xref="paper",
                yref="paper",
                x=-0.07,  # position to the left of the y-axis
                y=0.5,  # position in the middle of the y-axis
                xanchor="right",
                yanchor="middle",
                font=dict(size=20),
                textangle=-90  # rotate text for y-axis
            )
        ]
    )
    #fig.update_xaxes(title_text=varlabel[0], row=row_id, col=1)
    #fig.update_yaxes(title_text=varlabel[1], row=row_id, col=1)
    fig.update_traces(showscale=False)
    if show_fig:
        fig.show()
    write_fig(fig, f"{varname[0]}_vs_{varname[1]}")


def plot_cutflow():
    print("-- Plotting: cutflow", flush=True)
    # fig = go.Figure()
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    eff_mins, eff_maxs = np.array([]),  np.array([])
    rej_mins, rej_maxs = np.array([]), np.array([])
    for i, dataset in enumerate(DATASETS):
        dataset_dir = mini_tree_dir / dataset
        rootfile = dataset_dir / "cutflow.root"
        assert rootfile.exists(), f"{rootfile} not found"
        f = ROOT.TFile(str(rootfile), 'READ')
        hist = f.Get("cutflow")
        bins = np.array([hist.GetXaxis().GetBinLabel(i) for i in range(1, hist.GetNbinsX() + 1)])
        pass_values = np.array([hist.GetBinContent(i) for i in range(1, hist.GetNbinsX() + 1)])
        f.Close()
        del f
        # Calculate cut efficiency
        total_pass = pass_values[0]
        eff = []
        for npass in pass_values:
            eff.append(npass / total_pass)
        eff = np.array(eff)

        # Plot cutflow with Plotly
        if DATASETS_ISSIG_MAP[dataset]:
            # Plot Signals
            ymin, ymax = add_scatter(fig,DATASETS_LABEL_MAP[dataset],bins,eff,False)
            if ymin and ymax:
                eff_mins = np.append(eff_mins, ymin)
                eff_maxs = np.append(eff_maxs, ymax)
        else:
            ymin, ymax = add_scatter(fig,DATASETS_LABEL_MAP[dataset],bins,1/eff,True)
            if ymin and ymax:
                rej_mins = np.append(rej_mins, ymin)
                rej_maxs = np.append(rej_maxs, ymax)
    if eff_mins.size == 0:
        print("[Warning] No signal efficiency is found. Skip plotting cutflow.", flush=True)
        return
    if rej_mins.size == 0:
        print("[Warning] No background rejection is found. Skip plotting cutflow.", flush=True)
        return
    dy_eff = eff_maxs.max() - eff_mins.min()
    dy_rej = np.log10(rej_maxs.max()) - np.log10(rej_mins.min())
    plot_style(
        fig,
        fig_width=900,
        fig_height=675,
        fig_title='Cutflow',
        xaxes_title='',
        xaxes_tickangle=15,
        yaxes_title='Signal Efficiency',
        #yaxes_range=[eff_mins.min() - dy_eff * 0.1, eff_maxs.max() + dy_eff * PLOT_HEAD_MARGIN],
        yaxes_range=[0 - 0.1 ,1 + PLOT_HEAD_MARGIN],
    )
    fig.update_yaxes(
        title_text="Background Rejection",
        type="log",
        range=[np.log10(rej_mins.min()) - dy_rej * 0.1, np.log10(rej_maxs.max()) + dy_rej * PLOT_HEAD_MARGIN],
        secondary_y=True
    )
    fig.update_layout(
        legend=dict(
            x=0.93,y=1,
            groupclick="toggleitem",
            ),
    )
    if show_fig:
        fig.show()
    write_fig(fig, 'cutflow')


def plot_resolution():
    print(f"-- Plotting resolution of {varname}", flush=True)
    var_truth = RES_TRUTH_MAP[varname]
    var_delta = RES_DELTA_MAP[varname]
    varlabel = var_truth
    varbinning = RES_BINNING_MAP[varname]
    varunitconv = 1
    varcutid = 0
    if var_truth in VAR_LABEL_MAP:
        varlabel = VAR_LABEL_MAP[var_truth]
        varunitconv = VAR_UNITCONVERT_MAP[var_truth]

    cutflow_subdir = 'precut' if varcutid == 0 else f"cut{varcutid}"

    fig = go.Figure()
    ymins=np.array([])
    ymaxs=np.array([])
    for dataset in DATASETS:
        dataset_dir = mini_tree_dir / dataset
        if len(glob.glob(f"{dataset_dir}/{cutflow_subdir}/miniTree*.root")) == 0:
            print(f"[Warning] No miniTree is found for {dataset} in {cutflow_subdir}. Skip plotting.", flush=True)
            continue
        rdf = ROOT.RDataFrame("dp", f"{dataset_dir}/{cutflow_subdir}/miniTree*.root")
        ds_label = DATASETS_LABEL_MAP[dataset]
        # Loop over centers
        x_vals=[]
        y_vals=[]
        dx = (varbinning[2] - varbinning[1]) / varbinning[0]
        for center in np.arange(varbinning[1], varbinning[2] + dx, dx):
            histo = rdf.Filter(f"({var_truth} > {center - 0.5*dx}) && ({var_truth} < {center + 0.5*dx}) && ({FILTER_MAP[RES_FILTER_MAP[varname]]})").Histo1D(var_delta, "EventWeight")
            gaus = ROOT.TF1("gaus", "gaus", -dx, dx) 
            # Fit the histogram with the Gaussian function
            histo.Fit(gaus, "RQN")  # 'R' option to fit within the range specified in TF1
            # Retrieve the fitted parameters
            mean = gaus.GetParameter(1)  # Mean of the Gaussian
            sigma = gaus.GetParameter(2)  # Standard deviation (sigma) of the Gaussian
            if RES_RELATIVE_MAP[varname]:
                y_vals.append(100 * abs(sigma / center))
            else:
                y_vals.append(sigma)
            x_vals.append(center * varunitconv)
            
        fig.add_trace(
            go.Scatter(
                name=ds_label,
                x=x_vals,
                y=y_vals,
                mode='lines+markers',
                line=dict(width=2),
                #line_color='#7F7F7F' if dataset == 'inclusive' else None,
                marker=dict(
                    symbol=marker_symbol_cycle[len(fig.data) % len(marker_symbol_cycle)],
                ),
            ),
        )
    dy=max(y_vals) - min(y_vals)
    plot_style(
        fig,
        fig_title='',
        xaxes_title=varlabel,
        yaxes_title='Resolution' if RES_RELATIVE_MAP[varname] else 'σ',
        yaxes_range=[min(y_vals) - 0.1 * dy, max(y_vals) + PLOT_HEAD_MARGIN * dy],
        darkshine_label3=LABEL if LABEL else FILTER_LABEL_MAP[RES_FILTER_MAP[varname]]
    )
    if RES_RELATIVE_MAP[varname]:
        fig.update_yaxes(ticksuffix="%")
    if show_fig:
        fig.show()
    write_fig(fig, f"{varname}_{cutflow_subdir}")

if __name__ == '__main__':
    args = get_args()
    read_config("share/default_info.yaml")
    read_config(args.config)
    show_fig = args.show
    OUTPUT_DIR.parent.mkdir(parents=True, exist_ok=True)
    mini_tree_dir = OUTPUT_DIR / "miniTree"

    # Prepare miniTree
    ROOT.ROOT.EnableImplicitMT()
    if mini_tree_dir.exists():
        print(f"[INFO] Found existing miniTree directory {mini_tree_dir}", flush=True)
    if args.recreate or (not mini_tree_dir.exists()):
        process_dataset()

    # Cutflow
    if len(CUTFLOW) > 0:
        plot_cutflow()

    # Plot
    for varname in RESOLUTIONS:
        plot_resolution()

    for varname in set(HISTOCAT):
        plot_histoCategorical()

    for varname in set(HISTO1DS):
         plot_histo1D()
 
    for varname in HISTO2DS:
        plot_histo2Ds()

    gen_index_html(OUTPUT_DIR, JSON_LIST)
