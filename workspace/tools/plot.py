import plotly.graph_objects as go
import plotly.express as px
import numpy as np
from plotly.subplots import make_subplots


marker_symbol_cycle = ['circle', 'square', 'cross', 'star-square', 'star-diamond', 'pentagon',
                       'diamond-tall', 'diamond-wide', 'star', 'hourglass']

def plot_style(fig, fig_title=None,
               xaxes_title=None, yaxes_title=None,
               xaxes_type=None, yaxes_type=None,
               xaxes_tickangle=None,
               xaxes_range=None, yaxes_range=None,
               #fig_width=600, fig_height=600,
               fig_width=600, fig_height=600,
               fontsize1=20, fontsize2=18,
               darkshine_label1=r'<b><i>Dark SHINE</i></b>',
               darkshine_label2=r'3×10<sup>14</sup> events @ 4 GeV',
               darkshine_label3=None):
    axis_attr = dict(
        linecolor='#666666',  # '#2a3f5f',
        gridcolor='#F0F0F0',
        zerolinecolor='rgba(0,0,0,0)',
        linewidth=2,
        gridwidth=1,
        showline=True,
        #ticks='inside',
        tickwidth=2,
        tickcolor='#2a3f5f',
        mirror='ticks',
        tickfont=dict(
            #family='Times New Roman',
            #color='black',
            size=16,
        ),
    )
    fig.update_xaxes(
        **axis_attr,
        showgrid=False,
        range=xaxes_range,
        type=xaxes_type,
        tickangle=xaxes_tickangle,
        title=dict(
            text=xaxes_title,
            font=dict(
                #family='Times New Roman',
                #color='black',
                size=fontsize1,
            ),
        ),
    )
    fig.update_yaxes(
        **axis_attr,
        showgrid=True,
        showexponent='all',
        exponentformat='power',
        type=yaxes_type,
        range=yaxes_range,
        title=dict(
            text=yaxes_title,
            font=dict(
                #family='Times New Roman',
                #color='black',
                size=fontsize1,
            ),
        ),
    )
    fig.update_layout(
        title=dict(
            text=fig_title,
            x=0.5,
            font=dict(
                #family='Times New Roman',
                #color='black',
                size=fontsize1,
            ),
        ),
        legend=dict(
            x=0.98,
            y=0.99,
            xanchor='right',  # Anchor point of the legend ('left' or 'right')
            yanchor='top',  # Anchor point of the legend ('top' or 'bottom')
            bgcolor='rgba(0,0,0,0)',
            font=dict(
                size=fontsize2,
            ),
            groupclick="toggleitem",
        ),
        showlegend=True,
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)',
        margin=dict(
            l=90,
            r=20,
            b=20,
            t=80,
        ),
        width=fig_width,
        height=fig_height,
    )
    # Rearrange the legend items to form two columns
    n = len(fig.data)  # Number of traces
    if n > 6:  # (center+error) x 3
        fig.update_layout(
            legend=dict(
                orientation="h",
            )
        )
        nhalf = round(n/2)
        for i in range(nhalf):
            fig.data[i].legendgroup = 'group2'
        for i in range(nhalf, n):
                fig.data[i].legendgroup = 'group1'
    if darkshine_label1:
        fig.add_annotation(x=0.03, y=0.98,
                           xref="paper", yref="paper",
                           text=darkshine_label1,
                           font=dict(
                               #family='Times New Roman',
                               #color='black',
                               size=fontsize1 + 2,
                           ),
                           showarrow=False,
                           )
    if darkshine_label2:
        fig.add_annotation(x=0.03, y=0.91,
                           xref="paper", yref="paper",
                           text=darkshine_label2,
                           font=dict(
                               #family='Times New Roman',
                               #color='black',
                               size=fontsize2,
                           ),
                           showarrow=False,
                           )
    if darkshine_label3:
        fig.add_annotation(x=0.03, y=0.84,
                           xref="paper", yref="paper",
                           text=darkshine_label3,
                           font=dict(
                               #family='Times New Roman',
                               #color='black',
                               size=fontsize2,
                           ),
                           showarrow=False,
                           )

def add_histogram_data(fig, data, name, normalize=True, hist_bins=None, hist_range=None):
    if hist_range is None:
        data_min = min(data)
        data_max = max(data)
        margin = round(0.1 * (data_max - data_min))
        hist_range = (data_min - margin, data_max + margin)
    if hist_bins is None:
        hist_bins = round(hist_range[1] - hist_range[0])

    count, index = np.histogram(
        data,
        bins=hist_bins,
        range=hist_range,
        #density=normalize
    )

    if normalize:
        counts = counts / counts.sum()

    fig.add_trace(
        go.Scatter(
            name=name,
            x=index,
            y=count,
            line=dict(width=2, shape='hvh'),
        )
    )


def add_histo1d(fig, histo, name='hist', unitconv = 1, color=None, dash=None):
    n_bins = histo.GetNbinsX()
    edges = unitconv * np.array([histo.GetBinLowEdge(i) for i in range(1, n_bins + 2)])
    #dx = edges[1] - edges[0]
    #edges = edges + 0.5 * dx
    edges=(edges[1:] + edges[:-1])/2
    binvalue = np.array([histo.GetBinContent(i) for i in range(1, n_bins + 1)])
    errors = np.array([histo.GetBinError(i) for i in range(1, n_bins + 1)])
    y_upper = binvalue + errors
    y_lower = binvalue - errors
    # Set line and error band color from plotly color cycle
    if color is None:
        color_cycle=px.colors.qualitative.Plotly
        trace_index = int(len(fig.data) / 2)   # Divided by 2 when plot line and erro band
        color = color_cycle[trace_index % len(color_cycle)]
    if color[0] == '#':
        fill_color = f'rgba({int(color[1:3], 16)}, {int(color[3:5], 16)}, {int(color[5:7], 16)}, 0.1)'
    else:
        color_rgb = color.split('(')[1].split(')')[0].split(',')
        fill_color = f'rgba({color_rgb[0]}, {color_rgb[1]}, {color_rgb[2]}, 0.1)'
    fig.add_trace(
        go.Scatter(
            name=name,
            x=edges,
            y=binvalue,
            mode='lines',
            line=dict(width=2, shape='hvh', dash=dash),
            line_color=color,
            #fill='tozeroy',
            #fillcolor=fill_color,
        ),
    )
    # Retrieve the line color of the last trace added (the histogram line)
    line_color = fig.data[-1].line.color
    # Use the line color for the error band, but with added transparency
    if line_color[0] == '#':
        error_band_color = f'rgba({int(line_color[1:3], 16)}, {int(line_color[3:5], 16)}, {int(line_color[5:7], 16)}, 0.4)'
    else:
        line_color_rgb = line_color.split('(')[1].split(')')[0].split(',')
        error_band_color = f'rgba({line_color_rgb[0]}, {line_color_rgb[1]}, {line_color_rgb[2]}, 0.4)'
    fig.add_trace(
        go.Scatter(
            x=np.append(edges, edges[::-1]), # x, then x reversed
            y=np.append(y_upper, y_lower[::-1]), # upper, then lower reversed
            fill='toself',
            fillcolor=error_band_color,
            line=dict(color='rgba(255,255,255,0)', shape='hvh'),
            hoverinfo="skip",
            showlegend=False
        )
    )
    valuemin, valuemax = None, None
    if binvalue[binvalue>0].size > 0:
        valuemin = binvalue[binvalue>0].min()
        valuemax = binvalue[binvalue>0].max()
    return valuemin, valuemax

def plot_histo2d(histo, name='hist', unitconv = [1,1], normalize_mode=None):
    # Convert ROOT histogram to numpy
    n_bins_x, n_bins_y = histo.GetNbinsX(), histo.GetNbinsY()
    x_edges = np.array([histo.GetXaxis().GetBinLowEdge(i) for i in range(1, n_bins_x + 1)]) * unitconv[0]
    y_edges = np.array([histo.GetYaxis().GetBinLowEdge(i) for i in range(1, n_bins_y + 1)]) * unitconv[1]
    hist_values = np.array([[histo.GetBinContent(i, j) for i in range(1, n_bins_x + 1)] for j in range(1, n_bins_y + 1)])
    
    if normalize_mode == 'column':
        # Normalize the histogram by each x-bin
        column_sums = hist_values.sum(axis=0)
        if column_sums > 0:
            hist_values = 10* hist_values / column_sums
    if normalize_mode == 'row':
        # Normalize the histogram by each y-bin
        row_sums = hist_values.sum(axis=1)
        if row_sums > 0:
            hist_values = 10* hist_values / row_sums
    if normalize_mode == 'all':
        # Normalize the histogram by the total sum
        if hist_values.sum() > 0:
            hist_values = 10* hist_values / hist_values.sum()
    
    # Create plotly figure
    x_centers = (x_edges[1:] + x_edges[:-1]) / 2
    y_centers = (y_edges[1:] + y_edges[:-1]) / 2
    
    # Plot Root
    return go.Heatmap(
               name=name,
               x=x_centers,
               y=y_centers,
               z=np.log(hist_values),
               colorscale='Blues',
           )

def add_scatter(fig, name, bins, values, secondary_y = False):
    trace_index = len(fig.data)
    fig.add_trace(
        go.Scatter(
            name=name,
            x=bins,
            y=values,
            mode='markers',
            marker=dict(
                symbol=marker_symbol_cycle[trace_index % len(marker_symbol_cycle)],
                size=15,
                color='#7F7F7F' if name == 'Inclusive' else None,
                line=dict(
                    color='DarkSlateGrey',
                    width=2
                )
            ),
            opacity=0.8,
        ),
        secondary_y=secondary_y,
    )
    return values.min(), values[values != np.inf].max()

def plot_ratio(x, y1, y2, diff, diff_err=None, diff_err_band=None, labels=None, y1_err=None, y2_err=None,
               x_label: str = 'X', y_label: str = 'Yields',
               ratio_threshold: float = 5.0,
               annotation_text: str = None,
               figure_name: str = 'figure.svg'):
    fig = make_subplots(rows=2, cols=1,
                        shared_xaxes=True,
                        vertical_spacing=0.0,
                        row_heights=[0.75, 0.25])

    hit_ratio = len(x[diff > ratio_threshold])
    ratio_min = diff.min()
    ratio_max = diff.max() if not hit_ratio else ratio_threshold

    fig.add_trace(
        go.Scatter(
            x=x, y=y1, name=labels[0],
            # mode='lines',
            error_y=dict(
                type='data',
                symmetric=False,
                array=y1_err[0],
                arrayminus=y1_err[1]) if y1_err is not None else None
        ), row=1, col=1)
    fig.add_trace(
        go.Scatter(
            x=x, y=y2, name=labels[1],
            mode='markers',
            error_y=dict(
                type='data',
                symmetric=False,
                array=y2_err[0],
                arrayminus=y2_err[1]) if y2_err is not None else None
        ), row=1, col=1)

    fig.add_trace(go.Scatter(
        x=x[diff <= ratio_threshold], y=diff[diff <= ratio_threshold],
        name=f"{labels[1]}/{labels[0]}",
        # mode='markers',  #'markers+text',
        marker_color='black',
        marker_size=6,
        text=diff[diff <= ratio_threshold],
        textposition='bottom center',
        texttemplate='%{text:.5f}',
        error_y=dict(
            type='data',
            array=diff_err,
            color='black',
            thickness=1.5,
            width=3) if diff_err is not None else None,

    ), row=2, col=1)
    if diff_err_band:
        diff_upper = diff + diff_err_band
        diff_lower = diff - diff_err_band
        fig.add_trace(
            go.Scatter(
                x=np.append(x, x[::-1]),  # x, then x reversed
                y=np.append(diff_upper, diff_lower[::-1]),  # upper, then lower reversed
                fill='toself',
                fillcolor='rgba(0,0,0,0.25)',
                line=dict(color='rgba(255,255,255,0)'),
                hoverinfo="skip",
                showlegend=False
            ), row=2, col=1
        )

    fig.add_trace(go.Scatter(
        x=x[diff > ratio_threshold], y=diff[diff > ratio_threshold].where(diff <= ratio_threshold, ratio_threshold),
        name=f"{labels[1]}/{labels[0]} >={ratio_threshold}",
        mode='markers', marker_color='#3366CC', marker_symbol='arrow-up-open', marker_size=12, marker_line_width=2,
    ), row=2, col=1)

    fig.add_hline(y=1.0, line_width=1, line_dash="dash", line_color="grey", row=2, col=1)

    y_axis_attr = dict(
        linecolor="#666666",
        zerolinecolor='rgba(0,0,0,0)',
        linewidth=2,
        mirror=True,
        showgrid=False,
    )

    # fig.update_yaxes(**y_axis_attr, title_text=y_label, type="log", row=1, col=1)
    fig.update_yaxes(**y_axis_attr, title_text=y_label, row=1, col=1)
    fig.update_yaxes(**y_axis_attr, title_text=f"{labels[1]}/{labels[0]}", row=2, col=1,
                     range=[(ratio_min - (ratio_max - ratio_min) * 0.35) * 0.95, ratio_max * 1.05])


    x_axis_attr = dict(
        linecolor="#666666", gridcolor='#d9d9d9', zerolinecolor='rgba(0,0,0,0)', linewidth=2,
        showline=True, showgrid=True
    )

    fig.update_xaxes(**x_axis_attr, mirror=True, row=1, col=1)
    fig.update_xaxes(
        **x_axis_attr,
        mirror=False,
        title=dict(
            text=x_label,
            font=dict(
                size=18,
            ),
        ),
        row=2, col=1
    )

    # annotation
    if annotation_text is not None:
        fig.add_annotation(
            text=f'{annotation_text}',
            showarrow=False,
            align='left',
            xanchor='right', xref='paper', x=0.95,
            yref='paper', y=0.97,
            font=dict(size=18, family='Arial'),
        )

    fig.update_layout(
        legend=dict(
            orientation="h",
            yanchor="top",
            y=1.08,
            xanchor="right",
            x=1,
            font=dict(size=14),
        ),
        width=800,
        height=700,
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)',
    )

    return fig