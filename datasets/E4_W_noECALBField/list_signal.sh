#!/bin/bash
base=/lustre/collider/hepmc/DarkSHINE_Production/Baseline_1.6/e+_E4_W_noECALBField
for mass in 1 5 10 20 30 40 50 60 63 70 80 90 100 500 700 1000 1500 2000
do
    for proc in "sch" "tch" "brem"
    do
        file=${base}/signal_${proc}/dp_ana/merge_${mass}MeV.root
        if [[ -f ${file} ]]; then
            echo $file > e+_${proc}_${mass}MeV.txt
        fi
    done
done



base=/lustre/collider/hepmc/DarkSHINE_Production/Baseline_1.6/e-_E4_W_noECALBField/signal/dp_ana
for mass in 1 5 10 50 100 500 800 1000
do
    file=${base}/merge_${mass}MeV.root
    if [[ -f ${file} ]]; then
        echo $file > e-_signal_${mass}MeV.txt
    fi
done
