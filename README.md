[![pipeline status](https://gitlab.com/ykrsama/darkshine-analysis/badges/master/pipeline.svg )](https://gitlab.com/ykrsama/darkshine-analysis/-/commits/master)

[Analysis Plots](https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/positron_1MeV/)

[Slides](https://darkshine-analysis-ykrsama-1c4398ed5549bed87b570e88b7efabc7603d.gitlab.io/slides)

## Datasets:
List of DAna files: `./datasets`
Datasets information can be modified in `./tools/info.py`
